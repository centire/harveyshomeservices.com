<?php

// enqueue the child theme stylesheet

/*Function wp_schools_enqueue_scripts() {
wp_register_style( 'childstyle', get_stylesheet_directory_uri() . '/style.css'  );
wp_enqueue_style( 'childstyle' );
}
add_action( 'wp_enqueue_scripts', 'wp_schools_enqueue_scripts', 11);*/




/* ##### Harveys - enqueue custom stylesheet & scripts ##### */
function har_enqueue_style() {
	wp_register_style('style_har',get_site_url().'/har-media/css/style_har.css',array(),null);
	wp_enqueue_style('style_har');
}
function har_enqueue_script() {
	wp_register_script('script_har',get_site_url().'/har-media/scripts/script_har.js',array(),null);
	
	$gWP = array(
		'site_url' => get_site_url(),
		'ajax_url' => admin_url('admin-ajax.php')
	);
	wp_localize_script( 'script_har', 'gWP', $gWP );
	
	wp_enqueue_script('script_har');
}
add_action('wp_enqueue_scripts','har_enqueue_style',99999999);
add_action('wp_enqueue_scripts','har_enqueue_script',99999999);

add_action('login_enqueue_scripts','har_enqueue_style',99999999);




/* ##### Harveys - get post ##### */
add_action('wp_ajax_nopriv_ajax_action', 'har_ajax_loading');
add_action('wp_ajax_ajax_action', 'har_ajax_loading');

function har_ajax_loading() {
	$output = 'Error. No function specified.';
    switch($_REQUEST['fn']) {
        case 'har_get_post_meta':
			$postMeta = har_get_post_meta($_REQUEST['id']);
			$postTitle = array('title' => get_the_title($_REQUEST['id']));
			$output = array_merge($postMeta, $postTitle);
        break;
        case 'har_set_sms_sent':
			$output = har_set_sms_sent($_REQUEST['id']);
        break;
    }
    $output = json_encode($output);
    if(is_array($output)){print_r($output);}
    else{echo $output;}
    die;
}
function har_get_post_meta($id) {
	 $post = get_post_meta($id);
     return $post;
}
function har_set_sms_sent($id){
	$post = update_post_meta($id, 'wpcf-sms-sent', 1);
     return $post;
}




/* ##### Harveys - create new widget areas ##### */
function har_widgets_init() {
	register_sidebar( array(
		'name'          => 'Harveys Header Tagline',
		'id'            => 'har_header_tagline',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	) );
}
/*add_action( 'widgets_init', 'har_widgets_init' );*/
