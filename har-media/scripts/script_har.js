// JavaScript Document

if(typeof gWP == 'undefined'){
	console.log('Script localisation failed');
	var gWP = {
		'ajax_url':'http://harveyshomeservices.com/wp-admin/admin-ajax.php'
	};
}


if((window.navigator.userAgent.indexOf("MSIE ")>0)||(!!navigator.userAgent.match(/Trident.*rv\:11\./))){jQuery('body').addClass('ie');}

    
/* ##### SERVICE REQUEST FORM - SUCCESS PAGE ##### */
var gSMS = false;
if((window.location.href.indexOf('/request')!=-1)&&(window.location.search.indexOf('_target')!=-1)){

	/* Warn user against closing the window before SMS sent */
	gSMS = true;
	window.onbeforeunload = function(event) {
		var s;
		event = event || window.event;
		if (gSMS==true) {
			s = "Your service request is still being subitted. If you close the window now, it may not be completed.";
			event.returnValue = s;
			return s;
		}
	}
	
	var vID = window.location.search.split('_target=')[1].split('&')[0].split('#')[0];/*console.log(vID);*/
	
	if((typeof vID == 'number')||(/^\d+$/.test(vID))){
		jQuery.ajax({
			url: gWP.ajax_url,
			type: 'POST',
			data: {
				'action': 'ajax_action',
				'fn': 'har_get_post_meta',
				'id': vID
			},
			dataType: 'JSON',
			success:function(data){console.log('Get Post Meta | Success');console.log(data);
				
				var objData = (typeof data == 'object') ? data : (typeof data == 'string') ? {'Error: ':data} : JSON.parse(data);/*console.log(objData);*/
				
				var vSent = false;
				if((typeof objData['wpcf-sms-sent'] !== 'undefined')){console.log(objData['wpcf-sms-sent'][0]);
					if((parseInt(objData['wpcf-sms-sent'][0])==1)||(objData['wpcf-sms-sent'][0].toString=='true')){
						vSent = true;
						gSMS = false;
					}
				}console.log('SENT: '+vSent);
				
				/* SMS not sent - compose and sent, then update post */
				if(vSent!=true){
					
					/* Compose SMS message */
					
					/* Determine required service, or fall back to generic wording */
					var vMsg = "Hey HAR_RECIPIENT,HAR_BRHAR_BRA Harveys Home Services user has requested a callback regarding ";
					var vSvc = "one of our services";
					if((typeof objData['wpcf-service-type'] !== 'undefined')){switch(objData['wpcf-service-type'][0]){
						case 'locksmith':
							vSvc = "sending out a locksmith out to their home.";
						break;
						case 'bondclean':
							vSvc = "an urgent bond clean.";
						break;
						case 'pressureclean':
							vSvc = "pressure cleaning.";
						break;
						case 'handyman':
							vSvc = "sending out a handyman.";
						break;
						case 'graffiti':
							vSvc = "graffiti removal.";
						break;
						case 'security':
							vSvc = "installing a security service.";
						break;
						case 'other':
							if((typeof objData['wpcf-service-other'] !== 'undefined')){
								if(objData['wpcf-service-other'][0]!=''){vSvc = 'the following:HAR_BRHAR_BR"'+objData['wpcf-service-other'][0]+'"';}
							}
						break;
					}}
					vMsg += vSvc;
					
					/* Output contact details if provided */
					var vContact = '';
					if((typeof objData['title'] !== 'undefined')){
						if(objData['title']!=''){vContact += "HAR_BRName: "+objData['title'];}
					}
					if((typeof objData['wpcf-mobile'] !== 'undefined')){
						if(objData['wpcf-mobile'][0]!=''){vContact += "HAR_BRMobile: "+objData['wpcf-mobile'][0];}
					}
					if((typeof objData['wpcf-postcode'] !== 'undefined')){
						if(objData['wpcf-postcode'][0]!=''){vContact += "HAR_BRPostcode: "+objData['wpcf-postcode'][0];}
					}
					if(vContact!=''){vMsg += "HAR_BR"+vContact}
					
					vMsg += "HAR_BRHAR_BRVisit http://goo.gl/qi9Vox to see their full details.";/*console.log(vMsg);*/
					
					jQuery.post( "http://harveyshomeservices.com/har-media/twilio/action_sendSMS.php", { twlMsg: vMsg})
					.done(function( data ){console.log('Send Service Request SMS | Success');console.log(data);
						jQuery.ajax({
							url: gWP.ajax_url,
							type: 'POST',
							data: {
								'action': 'ajax_action',
								'fn': 'har_set_sms_sent',
								'id': 155
							},
							dataType: 'JSON',
							success:function(data){console.log('Set SMS Sent | Success');console.log(data);
								gSMS = false;
							},
							error: function(data){console.log('Set SMS Sent | Error');console.log(data);
								gSMS = false;
							}
						});		
					})
					.fail(function(data){
						console.log('Send Service Request SMS | Error');console.log(data);gSMS = false;
					});
				} else {
					gSMS = false;
				}
			},
			error: function(data){
				console.log('Get Post Meta | Error');console.log(data);
				gSMS = false;
			}
		});		
	} else {
		console.log('Error - invalid ID: '+vID);
		gSMS = false;
	}
	
}
/* ##### END SERVICE REQUEST FORM - SUCCESS PAGE ##### */




jQuery(document).ready(function($) {
    
	/* ##### WINDOW CHECK ##### */
	/* Determine window size & device type */
	var gMinWt = 990;
	var gWinWt = $(window).width();
	var gWinHt = $(window).height();
	
	var vUA = navigator.userAgent.toLowerCase();
	var gMobile = (/android|webos|iphone|ipad|ipod|blackberry/i.test(navigator.userAgent));
	var gTablet = ((/ipad/i.test(navigator.userAgent))||((/android/i.test(navigator.userAgent))&&(navigator.userAgent.indexOf('mobile')==-1)));
	
	function fnHar_windowCheck(){
		gWinWt = $(window).width();
		gWinHt = $(window).height();
				
		$('body').removeClass('xs sm md lg mobile tablet ie');
			 if(gWinWt<=768)	{$('body').addClass('xs');}
		else if(gWinWt<=992)	{$('body').addClass('sm');}
		else if(gWinWt<=1200)	{$('body').addClass('md');}
						else	{$('body').addClass('lg');}
	
		if(!!gMobile){$('body').addClass('mobile');}
		if(!!gTablet){$('body').addClass('tablet');}
		
		if((window.navigator.userAgent.indexOf("MSIE ")>0)||(!!navigator.userAgent.match(/Trident.*rv\:11\./))){$('body').addClass('ie');}
	}
	fnHar_windowCheck();
	/* ##### END WINDOW CHECK ##### */
	
	
	/* ##### MODULE: EQUALISED/VC COLUMNS ##### */
	/* Equalise column heights */
	var vTimer_colsEq = null;
	function fnHar_colsEq(){
		var arrRows = $('.vc_row.har-cols-eq,.vc_row.har-cols-vc');
		if((!$('body').hasClass('xs'))&&(arrRows.length>0)){/*console.log('Found '+arrRows.length+' rows demanding equality');*/
			arrRows.children('.full_section_inner,.vc_row-inner').each(function(index, element) {
				
				var arrCol_outer = $(this).children('.vc_column_container').children('.vc_column-inner');
				var arrCol_inner = arrCol_outer.children('.wpb_wrapper');
				
				if(arrCol_inner.length>1){/*console.log('Equalise this:');console.log($(this));*/
					var vHt = 0;
					arrCol_inner.each(function(index, element){vHt = Math.max(vHt,$(this).outerHeight());});/*console.log($(this).parent());console.log(vHt);*/
					arrCol_outer.css('min-height',vHt+'px');
					$(this).parent().addClass('har-cols-equalised');
				}
				
			});
			
			if(vTimer_colsEq==null){
				vTimer_colsEq = window.setInterval(function(){
					fnHar_colsEq();
				},5000);
			}
			
		}
	}
	fnHar_colsEq();
	/* ##### END MODULE: EQUALISED/VC COLUMNS ##### */
	
	
	/* ##### MODULE: TWILIO BUTTONS ##### */
	$('.har-row-twilio .qbutton').each(function(index, element) {
		if($(this).children('span').length==0){
	        $(this).html('<span>'+$(this).text()+'</span>');
		}
    });
	
	function fnHar_serviceRequest(argSvc){
		var vSvc = encodeURIComponent(argSvc);
		var vSpecs = '';
			vSpecs += 'width=800,';
			vSpecs += 'height=600,';
			vSpecs += 'left='+Math.max(0,parseInt(0.5*($(window).width()-600)))+',';
			vSpecs += 'top='+Math.max(0,parseInt(0.5*($(window).height()-800)))+',';
			vSpecs += 'location=0,menubar=0,resizable=1,scrollbars=1,status=0,title=0,titlebar=0,toolbar=0';/*console.log(vSpecs);*/
		window.open('http://harveyshomeservices.com/request/?service='+vSvc,'harServiceRequest',vSpecs);
	}
	
	$('a[href*="#/quote/"]').click(function(e) {
        var vSvc = encodeURIComponent($(this).attr('href').split('quote/')[1].replace(/\/$/,''));
		if((!!vSvc)&&(vSvc!='')){
			e.preventDefault();
			fnHar_serviceRequest(vSvc);
			return false;
		}
    });
	/* ##### END MODULE: TWILIO BUTTONS ##### */
	
	
	
	/* ##### WINDOW RESIZE ##### */
	/* Functions to run on resize */
	function fnHar_runOnResize(){
		fnHar_windowCheck();
		fnHar_colsEq();
	}
	$(window).resize(function(e){
		fnHar_runOnResize();
	});
	fnHar_runOnResize();
	/* ##### END WINDOW RESIZE ##### */
	
	
	
	/* ##### EVENT LISTENERS ##### */
	$('a[href^="#"]').click(function(e){
		var vHash = $(this).attr('href').replace('#','');
		if(!!$('#'+vHash)){
			e.preventDefault();
			$('html, body').animate({scrollTop:$('#'+vHash).offset().top - $('header').eq(0).height()}, 1000);
			return false;
		}
		else if(!!$('*[name="'+vHash+'"]')){
			e.preventDefault();
			$('html, body').animate({scrollTop:$('*[name="'+vHash+'"]').eq(0).offset().top - $('header').eq(0).height()}, 1000);
			return false;
		}
		
		if(vHash.replace(/\//g,'')=='services'){
			$('html, body').animate({scrollTop: $(".har-row-twilio").eq(0).offset().top-$('header').eq(0).height()}, 1000);
		}
	});
	
	/* ##### END EVENT LISTENERS ##### */
	
	
	
	
	
	$(window).load(function(e) {
    	fnHar_runOnResize();
	});
});

