<?php
set_time_limit(-1);
$response = [
    'output' => []
];

$branchTo = isset($_REQUEST['branchTo']) && $_REQUEST['branchTo'] ? $_REQUEST['branchTo'] : 'master';
$branchFrom = isset($_REQUEST['branchFrom']) && $_REQUEST['branchFrom'] ? $_REQUEST['branchFrom'] : 'master';

$commands = [
  //  'git status',
];

if(isset($_REQUEST['commit']))
{
    $commands = array_merge($commands, [
        'git init',
        'git remote add origin git@bitbucket.org:centire/harveyshomeservices.com.git',
        'git add -A',
        'git config  user.email "nivesh@centire.in"',
        'git config  user.name "Nivesh"',
        'git commit -m "Server Commit"',
        //'git push origin ' . $branchTo,
    ]);
}

if (isset($_REQUEST['before-commands'])) {
    $beforeCommands = explode(",", $_REQUEST['before-commands']);
    $commands = array_merge($commands, $beforeCommands);
}

# The commands
if (isset($_REQUEST['deploy'])) {
    $commands[] = 'git fetch --all';
    $commands[] = 'git reset --hard';
    $commands[] = 'git clean -f -d';
    $commands[] = 'git checkout ' . $branchTo;
    $commands[] = 'git pull origin ' . $branchFrom;
    $commands[] = 'git push origin ' . $branchTo;
}


if (isset($_REQUEST['after-commands'])) {
    $afterCommands = explode(",", $_REQUEST['after-commands']);
    $commands = array_merge($commands, $afterCommands);
}


# Run the commands for output
foreach ($commands AS $command) {
    if (strpos($command, 'cd ') === 0) {
        chdir(str_replace('cd ', '', $command));
    } else {
        # Run it
        $tmp = (shell_exec($command . ' 2>&1'));
        # Output
        $response['output'][$command] = trim($tmp);
    }
}


header("Content-type:application/json");
echo json_encode($response);
die;

//http://18.217.180.183/deploy.php?before-commands=git%20clone%20git@bitbucket.org:centire/erp.git,cd%20erp,composer%20install&branchFrom=development&branchTo=development


//http://18.217.180.183/deploy.php?dir=erp&branchFrom=development&branchTo=development&after-commands=composer%20install,php%20artisan%20migrate,php%20artisan%20db:seed